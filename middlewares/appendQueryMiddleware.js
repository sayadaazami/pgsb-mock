import {getEntityBy} from "../utils/entityManager";

const APPEND = {
    'get': {
        officeServices: async (req) => {
            if (!req.auth.isAdmin) {
                const office = await getEntityBy('offices', 'user.id', req.authId, {});
                req.query.officeId = office.id ? String(office.id) : null;
            }
        },
        serviceLogs: async (req) => {
            if (!req.auth.isAdmin) {
                const office = await getEntityBy('offices', 'user.id', req.authId, {});
                req.query['office.id'] = office.id ? String(office.id) : null;
            }
        },
        transactions: async (req) => {
            if (!req.auth.isAdmin) {
                const office = await getEntityBy('offices', 'user.id', req.authId, {});
                req.query.officeId = office.id ? String(office.id) : null;
            }
        }
    }
}

const appendParams = async (req) => {
    const method = req.method.toLowerCase();
    const path = req.path.replace('/', '');
    if (APPEND[method] && APPEND[method][path])
        await APPEND[method][path](req);
}

export const appendQueryMiddleware = async (req, res, next) => {
    if (!req.auth) return next();
    await appendParams(req);
    next();
};
