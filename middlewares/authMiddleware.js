export const authMiddleware = (req, res, next) => {
    if (req.auth) next();
    else res.status(401).send(res.jsonp({
        message: 'Unauthorized'
    }));
};
