import jwt_decode from "jwt-decode";
import {http} from "../utils/http";

export const jwtMiddleware = (req, res, next) => {
    try {
        const token = req.headers.authorization;
        req.auth = token ? jwt_decode(token) : null;
        if (req.auth) {
            req.auth.id = req.authId = req.auth.sub;
            req.auth.isAdmin = req.isAdmin = req.auth.realm_access.roles.includes('super_admin');
            http.defaults.headers['Authorization'] = token;
        }
    } catch (e) {
        req.auth = null;
    }
    next();
};
