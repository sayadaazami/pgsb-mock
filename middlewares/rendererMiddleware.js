import qs from 'querystring';
import {exportData} from "../utils/helpers";

export const rendererMiddleware = (req, res) => {
    const params = qs.parse(req.url.split('?')[1] || '');
    if (params._page || params._limit) {
        const totalSize = +res.get("X-Total-Count"),
            size = +(params._limit || totalSize),
            totalPage = Math.ceil(totalSize / size),
            page = +(params._page || 1);
        if (params._export) { //TODO باید سر ستون اینا با فرانتیا چک بشه
            const result = exportData(res.locals.data, params._export),
                downloadName = 'download.xls';
            res.setHeader('Content-Length', result.length);
            res.setHeader('Content-Type', 'appli');
            res.writeHead(200, {
                "Content-Disposition": "attachment;filename=" + downloadName,
                'Content-Type': 'application/vnd.ms-excel',
                'Content-Length': result.length
            });
            res.write(result, 'binary');
            res.end();
        } else
            res.jsonp({
                content: res.locals.data,
                meta: {page, totalPage, size, totalSize},
            });
    } else
        res.jsonp(res.locals.data);
};
