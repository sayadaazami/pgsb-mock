import {getEntityFromRequest} from "../utils/entityManager";

export const timestampsMiddleware = async (req, res, next) => {
    if (req.method === "POST") {
        req.body.updated = req.body.created = Date.now();
        next();
    } else if (req.method === "PUT") {
        const entity = await getEntityFromRequest(req);
        req.body = {
            ...entity,
            ...req.body,
            updated: Date.now()
        }
        next()
    } else
        next();
};
