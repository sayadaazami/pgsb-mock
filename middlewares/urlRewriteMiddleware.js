import {toCamelCaseUrl} from "../utils/helpers";

export const urlRewriteMiddleware = (req, res, next) => {
    req.url = req.url.replace('/api', '')
        .replace('/base-data', '')
        .replace('/report', '')
    req.url = toCamelCaseUrl(req.url, 1);
    next();
};
