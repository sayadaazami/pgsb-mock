import jsonServer from "json-server";
import path from "path";
import {fileURLToPath} from 'url';
import {authMiddleware} from "./middlewares/authMiddleware";
import {rendererMiddleware} from "./middlewares/rendererMiddleware";
import {timestampsMiddleware} from "./middlewares/timestampsMiddleware";
import {urlRewriteMiddleware} from "./middlewares/urlRewriteMiddleware";
import {mediaUploader} from "./services/uploader/uoloader";
import {jwtMiddleware} from "./middlewares/jwtMiddleware";
import {appendQueryMiddleware} from "./middlewares/appendQueryMiddleware";
import {serviceCallManager} from "./services/service-call/serviceCallManager";
import {SERVER_PORT} from "./utils/constats";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const server = jsonServer.create();
const router = jsonServer.router(path.join(__dirname, "database/db.json"));

router.render = rendererMiddleware;
server.use([
    ...jsonServer.defaults({
        logger: true,
        bodyParser: true,
        noCors: true,
    }),
    jsonServer.bodyParser,
    jwtMiddleware,
    authMiddleware,
]);
mediaUploader(server);
serviceCallManager(server);
server.use([
    urlRewriteMiddleware,
    appendQueryMiddleware,
    timestampsMiddleware,
    router,
]);

server.listen(SERVER_PORT, () => {
    console.log(`JSON Server is running on http://localhost:${SERVER_PORT}`);
});

