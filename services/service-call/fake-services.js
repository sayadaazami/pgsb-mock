export const FAKE_SERVICES = [
    {
        id: 'sabte-ahval',
        name: "ثبت احوال",
        url: "https://sabt-ahval.com/api",
        site: "https://sabt-ahval.com",
        documents: "",
        process: "این سرویس نیاز به فرایند خاصی ندارد ",
        info: "دریافت اطلاعات هویتی از ثبت احوال",
        fields: {
            "nationalCode": {title: "کد ملی", regex: "\\d{10}", required: true}
        }
    },
    {
        id: 'bargh',
        name: "پرداخت قبض برق",
        url: "https://barq.ir/api/payment",
        site: "https://barq.ir",
        documents: "تصویر قبض برق",
        process: "این سرویس نیاز به فرایند خاصی ندارد ",
        info: "پرداخت قبض برق به صورت آنلاین",
        fields: {
            "shenase-ghabz": {title: "شناسه قبض", regex: "^\\d{10}$", required: true},
            "shenase-pardakht": {title: "شناسه پرداخت", regex: "^\\d{10}$", required: true},
            "pardakht-konanade": {title: "نام پرداخت کننده", regex: "^[^0-9!@#$%^&*()_=.,;`]*$", required: false},
        }
    }
]

export const FAKE_SERVICE_CALLS = {
    'sabte-ahval': () => {
        return {
            "نام": "محمد",
            "نام خانوادگی": "مرادیخواه",
            "نام پدر": "علی",
            "تاریخ تولد": "1368/01/01",
        }
    },
    'bargh': (data) => {
        return {
            "نتیجه": "با موفقیت پرداخت شد",
            "کد رهگیری": "123456789"
        }
    }
}
