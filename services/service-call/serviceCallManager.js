import {addEntity, getEntitiesBy, getEntity, getEntityBy, getEntityByParams} from "../../utils/entityManager";
import {FAKE_SERVICE_CALLS, FAKE_SERVICES} from "./fake-services";
import {ACCOUNT_TITLES} from "../../utils/constats";
import {applyMapper, exportData, getObjectValue} from "../../utils/helpers";

export function serviceCallManager(server) {
    server.get('/api/services/list', (req, res) => {
        res.jsonp(FAKE_SERVICES)
    });

    server.get('/api/services/:id', async (req, res) => {
        const service = await getEntity('services', req.params.id);
        if (!service)
            return res.status(404).jsonp({message: 'service not found'});

        const accounts = await getEntitiesBy('accounts', {}, []);
        const share = service.share.map(x => ({
            ...x,
            title: isNaN(+x.id) ? ACCOUNT_TITLES[x.id] : accounts.find(a => a.id === x.id).name
        }));

        return res.jsonp({
            ...service,
            share
        });
    });

    server.post('/api/services/:id/call', async (req, res) => {
        const service = await getEntity('services', req.params.id);
        if (!service)
            return res.status(404).jsonp({message: 'service not found'});
        const serviceDetail = FAKE_SERVICES.find(x => x.name === service.name)
        const result = FAKE_SERVICE_CALLS[serviceDetail.id]();
        if (!result)
            return res.status(404).jsonp({message: 'fake service not found'});
        const office = await getEntityBy('offices', 'user.id', req.authId);
        if (!office)
            return res.status(404).jsonp({message: 'office not found'});

        const accounts = await getServiceCallAccounts(req, res, service, office);
        if (typeof accounts === 'string')
            return res.status(404).jsonp({message: accounts});
        addTransactionLog(service, office, req.auth, accounts).then(transactions => {
            addServiceLog(service, office, req.body, result).then(log => {
                res.jsonp({...log, result})
            }).catch(err => {
                res.status(500).jsonp({"message": err.message})
            });
        });
    });

    const downloadHandler = async (req, res) => {
        const {fields, kind} = req.body,
            items = await getEntitiesBy(req.url.replace('/api', '').replace('/download', ''), {
                ...req.query,
                _page: 1,
                _limit: 10000
            });
        const data = items.map(item =>
            Object.keys(fields).reduce((result, key) => {
                return {
                    ...result,
                    [fields[key].title || key]: applyMapper(getObjectValue(item, key), fields[key].format)
                }
            }, {}));

        const result = exportData(data, kind),
            downloadName = `download.${kind === 'PDF' ? 'pdf' : 'xls'}`;
        res.setHeader('Content-Length', result.length);
        res.writeHead(200, {
            "Content-Disposition": "attachment;filename=" + downloadName,
            'Content-Type': 'application/octet-stream',
            'Content-Length': result.length
        });
        res.write(result, 'binary');
        res.end();
        res.jsonp(data);
    }

    server.post('/api/:entityName/download', downloadHandler);
    server.post('/api/report/:entityName/download', downloadHandler);
}

async function addTransactionLog(service, office, auth, accounts) {
    let items = [],
        description = 'فراخوانی خدمت ' + service.name;
    let tax = 0, {tax: taxAccount, ...otherAccounts} = accounts;

    for (let index in otherAccounts) {
        const share = service.share.find(x => String(x.id) === String(index));
        const transactionData = transactionLogCalculator(otherAccounts[index].id, service, office, share.value, 9, description)
        await addEntity('transactions', transactionData);
        tax += transactionData.tax;
        items.push(transactionData)
    }

    const transactionData = transactionLogCalculator(taxAccount.id, service, office, tax, 0, description)
    await addEntity('transactions', transactionData);

    return Promise.resolve(items);
}

function transactionLogCalculator(accountId, service, office, value, tax, title) {
    let taxVal = tax ? Math.ceil(value / 100 * tax) : 0;
    if (taxVal && taxVal < 50) taxVal = 50;
    return {
        accountId: accountId,
        serviceId: service.id,
        officeId: office.id,
        description: `${title || 'سهم'} ${service.name}`,
        amount: value - taxVal,
        tax: taxVal,
    };
}

function addServiceLog(service, office, params, result) {
    return addEntity('serviceLogs', {
        "office": {
            "id": office.id,
            "name": office.name
        },
        "service": {
            "id": service.id,
            "name": service.name
        },
        params: params ? Object.keys(params).reduce((res, c) => ({
            ...res,
            [c]: {value: params[c], title: service.fields[c].title}
        }), {}) : {},
        result: result || {}
    })
}

async function getServiceCallAccounts(req, res, service, _office) {
    const office = await getEntityByParams('accounts', {id: _office.accountId, kind: 'office'});
    if (!office)
        return 'office account not found';

    const _provider = await getEntityByParams('providers', {id: service.providerId});
    if (!_provider)
        return 'provider not found';
    const provider = await getEntityByParams('accounts', {id: _provider.accountId, kind: 'provider'});
    if (!provider)
        return 'provider account not found';

    const _supplier = await getEntityByParams('suppliers', {id: service.supplierId});
    if (!_supplier)
        return 'supplier not found';
    const supplier = await getEntityByParams('accounts', {id: service.supplierId, kind: 'supplier'});
    if (!supplier)
        return 'supplier account not found';

    const tax = await getEntityByParams('accounts', {kind: 'tax'});
    if (!tax)
        return 'tax account not found';

    let result = {
        office,
        provider,
        supplier,
        tax,
    }

    const others = service.share.filter(x => !Number.isNaN(+x.id));
    for (let item of others)
        result[item.id] = await getEntityByParams('accounts', {id: item.id});

    return result;
}
