import fileUpload from 'express-fileupload'
import {fileURLToPath} from "url";
import path from "path";
import fs from "fs";
import {createDir} from "../../utils/helpers";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.join(path.dirname(__filename), 'uploads');
createDir(__dirname);

export function mediaUploader(server) {
    server.use(fileUpload({
        createParentPath: true
    }));

    server.post('/api/media', (req, res) => {
        try {
            if (!req.files) {
                res.status(400).send({status: false, message: 'No file uploaded'});
            } else {
                const file = req.files.file,
                    ext = path.extname(file.name),
                    fileName = uuidv4() + ext;
                file.mv(path.join(__dirname, fileName));
                res.status(200).send({
                    status: true,
                    name: fileName,
                    mimetype: file.mimetype,
                    size: file.size
                });
            }
        } catch (err) {
            res.status(500).send(err);
        }
    });

    server.get('/api/media/:name', (req, res) => {
        const filePath = path.join(__dirname, req.params.name);
        if (!fs.existsSync(filePath))
            res.status(404).send('file not found');
        else {
            var file = fs.readFileSync(filePath, 'binary');
            res.setHeader('Content-Length', file.length);
            res.write(file, 'binary');
            res.end();
        }
    });
}
