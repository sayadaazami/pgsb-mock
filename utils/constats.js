export const SERVER_PORT = process.env.PORT || 5005;
export const INTERNAL_URL = `http://localhost:${SERVER_PORT}/api/`;

export const ACCOUNT_TITLES = {
    supplier: 'تامین کننده',
    provider: 'ارایه دهنده',
    office: 'دفتر پیشخوان',
}
