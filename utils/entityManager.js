import {http} from "./http";

// region getters
export function getEntityNameFromPath(path) {
    return path.match(/^\/(.*)\//)[1];
}

export function getEntityIdFromPath(path) {
    return path.match(/^\/.*\/(\d+)/)[1];
}

export async function getEntityFromRequest(req, defaultValue = null) {
    return await getEntity(getEntityNameFromPath(req.url), getEntityIdFromPath(req.url), {}) || defaultValue;
}

export async function getEntity(entityName, id, defaultValue = null) {
    return await getEntityBy(entityName, 'id', id, defaultValue);
}

export async function getEntityBy(entityName, key, value, defaultValue = null) {
    const entities = await getEntitiesBy(entityName, {[key]: value});
    if (entities[0])
        return entities[0];
    return defaultValue;
}

export async function getEntityByParams(entityName, params, defaultValue = null) {
    const entities = await getEntitiesBy(entityName, params);
    if (entities[0])
        return entities[0];
    return defaultValue;
}

export async function getEntitiesBy(entityName, params, defaultValue = []) {
    const query = Object.keys(params).map(key => `${key}=${params[key]}`).join('&');
    try {
        const entity = await http.get(`${entityName}?${query}`);
        if (entity.data)
            return (entity.data ? (entity.data.content || entity.data) : defaultValue) || defaultValue;
    } catch (e) {
    }
    return defaultValue;
}

// endregion getters

// region setters
export function addEntity(entityName, data) {
    return http.post(entityName, data)
        .then(response => {
            if (response.status >= 200 && response.status < 299)
                return response.data;
            return Promise.reject({
                code: response.status,
                message: response.data.message || response.statusText
            })
        })
        .catch(e => {
            console.log(e)
            return Promise.reject(e);
        })
}

// endregion setters
