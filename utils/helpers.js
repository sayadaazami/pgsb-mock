import fs from "fs";
import json2xls from "json2xls";
import momentJ from "jalali-moment";
import {messages} from "./messages";


//region string
export function snakeToCamelCase(text) {
    if (!text.length) return text;
    const str = text.split("-").map(substr => substr.charAt(0).toUpperCase() + substr.slice(1)).join("");
    return str[0].toLowerCase() + str.substr(1);
}

export function toCamelCaseUrl(url, depth = 100) {
    const parts = url.replace(/^\/+/, '').split("/");
    for (let i in parts) {
        if (i >= depth) break;
        parts[i] = snakeToCamelCase(parts[i]);
    }
    return '/' + parts.join("/");
}

//endregion string

export function createDir(path) {
    try {
        if (!fs.existsSync(path))
            fs.mkdirSync(path);
        return true;
    } catch (e) {
        console.log(e)
        return false;
    }
}

export function uuidv4() {
    var u = '', i = 0;
    while (i++ < 36) {
        var c = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'[i - 1], r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        u += (c == '-' || c == '4') ? c : v.toString(16)
    }
    return u;
}

export function exportData(data, type) {
    var xls = json2xls(data, {});
    return xls;
}

export function getObjectValue(obj, eky) {
    const arr = eky.split(".");
    while (arr.length) {
        (obj = obj[arr.shift()])
    }
    return obj;
}

export function toPersianDate(date, format = 'YYYY/MM/DD') {
    try {
        return date ? momentJ().locale('fa').format(format) : '';
    } catch (e) {
        return date;
    }
}

export function trans(value) {
    return messages[value] || value;
}

export function applyMapper(value, formatter) {
    if (value === undefined) return '--';
    switch (formatter) {
        case 'date':
            return toPersianDate(value)
        default:
            return typeof value === 'string' ? trans(value) : value;
    }
}
