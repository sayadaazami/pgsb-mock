import axios from "axios";
import {INTERNAL_URL} from "./constats";

const http = axios.create({
    baseURL: INTERNAL_URL,
})

export {http};
