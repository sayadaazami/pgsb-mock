const KcAdminClient = require('@keycloak/keycloak-admin-client').default;

const KEYCLOAK_REALM = process.env.KEYCLOAK_REALM || 'pgsb';
const KEYCLOAK_URL = process.env.KEYCLOAK_URL || 'https://dev.sso.hamrasta.com/';
const KEYCLOAK_CLIENT_ID = process.env.KEYCLOAK_CLIENT_ID || 'user-management';
const KEYCLOAK_CLIENT_SECRET = process.env.KEYCLOAK_CLIENT_SECRET || 'IzbxInJZKerZi5Jbac4x4Bg7qQszwokl';

const keycloakClient = new KcAdminClient({baseUrl: KEYCLOAK_URL, realmName: KEYCLOAK_REALM});
// await keycloakClient.auth({
//     grantType: 'client_credentials',
//     clientId: KEYCLOAK_CLIENT_ID,
//     clientSecret: KEYCLOAK_CLIENT_SECRET
// });

export {keycloakClient};
